'use strict'
const mongoose = require('mongoose')
const app = require('./app')

mongoose.Promise = global.Promise

app.listen(config.port, () => {
	console.log(`Aplicacion escuchando en http://0.0.0.0:${config.port}`)
})

/*mongoose.connection.openUri(config.db, (err) => {
	if (err) throw err
	console.log('Conexion a la base de datos establecida...')
})*/