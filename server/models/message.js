'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MessageSchema = new Schema({
	text: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		default: Date.now
	},
    imageUrl: {
        type: String,
    },
	_creator: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	}
})

module.exports = mongoose.model('Message', MessageSchema)