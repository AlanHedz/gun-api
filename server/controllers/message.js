'use stric'

const Message = require('../models/message')
const cloudinary = require('cloudinary')

function getMessages (req, res) {
    Message.find({})
        .populate('_creator')
        .sort({ created: 'desc' })
        .exec((err, messages) => {
            if (err) res.status(500).send({ message: `Error al realizar la peticion: ${err}` })
            if (!messages) res.status(404).send({ message: `No existen mensajes` })
            res.status(200).send(messages)
        })
}

function saveMessage (req, res) {
    let message = new Message()
    message.text = req.body.text
    message._creator = req.body.user_id
    if (req.file != undefined) {
        cloudinary.uploader.upload(req.file.path, function (result) {
            message.imageUrl = result.url
            message.save((err, messageStored) => {
                if (err) res.status(500).send({ message: `Error al guardar el mensaje: ${err}` })
                res.status(200).send({ message: 'Se ha enviado tu noticía.' })
            })
        })
    } else {
        message.save((err, message) => {
            if (err) res.status(500)
            res.status(200).send({ message: 'Se ha enviado tu noticía' })
        })
    }
}

module.exports = {
    getMessages,
    saveMessage
}