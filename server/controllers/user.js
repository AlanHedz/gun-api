'use strict'

const User = require('../models/user')
const fs = require('fs')

function profile (req, res) {
	res.render('users/profile', { message: req.flash('profileMessage') })
}

function updateProfile (req, res) {
    let user_id = req.user._id
    User.findById(user_id, (err, user) => {
        if (err) {
            console.log(err)
            return res.redirect('/users/profile')
        }
        if (req.file) {
            fs.rename(req.file.path, 'public/avatars/' + req.file.originalname)
            user.local.avatar = req.file.originalname
        }
        user.local.username = req.body.username
        user.save((err, userUpdate) => {
            if (err) {
                console.log(err)
                res.redirect('/users/profile')
            }
            return res.redirect('/homepage')
        })
    })
}

function getUser (req, res) {
	if (req.isAuthenticated) {
		return res.send(req.user)
	}
}

function isAuth (req, res) {
	if (req.isAuthenticated()) {
		return res.send(true)
	}
	return res.send(false) 
}

module.exports = {
    profile,
    updateProfile,
    isAuth,
    getUser,
}