function home (req, res) {
    if (req.isAuthenticated()) {
        return res.redirect('/homepage')
    }
    res.render('home/index', { message: req.flash('Message') })
}

function homepage(req, res) {
    if (!req.isAuthenticated()) {
        return res.redirect('/')
    }
    res.render('home/home', { message: req.flash('Message') })
}

module.exports = {
    home,
    homepage
}