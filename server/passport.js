const LocalStrategy = require('passport-local').Strategy

const User = require('./models/user')

module.exports = function (passport) {

	passport.serializeUser((user, done) => {
		done(null, user.id)
	})

	passport.deserializeUser((id, done) => {
		User.findById(id, (err, user) => {
			done(err, user)
		})
	})

	//Registrar usarios
	passport.use('local-signup', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	}, (req, email, password, done) => {

        console.log(email)

		User.findOne({'local.email': email}, (err, user) => {
			if (err) {
                return done(err) 
            }
			if (user) { 
				return done(null, false, req.flash('Message', 'El email ya existe, intenta con otro.')) 
			} else {
				let newUser = new User()
				newUser.local.email = email
				newUser.local.username = req.body.username
				newUser.local.password = newUser.generateHash(password)
				newUser.save((err) => {
					if (err) {
                        throw err
                        console.log(err)
                    }
					return done(null, newUser)
				})
			}
		})
	}))
	passport.use('local-login', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	}, (req, email, password, done) => {
		User.findOne({'local.email': email}, (err, user) => {
			if (err) { return done(err) }
			if (!user) { 
				return done(null, false, req.flash('Message', 'No se ha encontrado el usuario o la contraseña')) 
			}
			if (!user.validatePassword(password)) {
				return done(null, false, req.flash('Message', 'El email y la contraseña no coinciden.'))
			}
			return done(null, user)
		})
	}))

}