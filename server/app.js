'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const flash = require('connect-flash')
const passport = require('passport')
const session = require('express-session')
const app = express()
const cloudinary = require('cloudinary')

const homeRoutes = require('./routes/home')
const userRoutes = require('./routes/users')
const messageRoutes = require('./routes/messages')
const config = require('./config')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

cloudinary.config({ 
  cloud_name: 'a-plus',
  api_key: '794319436982998', 
  api_secret: 'oyxaiPmsisWVyAG50nTqlBhoxhQ' 
});

app.set('view engine', 'pug')
app.use(express.static('public'))
app.use(cookieParser())

app.use(session({
    secret: 'reactrr',
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

const passportRoutes = require('./routes/passport_routes')(app, passport)

require('./passport')(passport)

app.use('/', homeRoutes)
app.use('/users', userRoutes)
app.use('/', messageRoutes)


module.exports = app