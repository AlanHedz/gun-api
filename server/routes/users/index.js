'use strict'

const express = require('express')
const UserCtrl = require('../../controllers/user')
const auth = require('../../middlewares/auth')
const multer = require('multer')
const uploader = multer({ dest: './public/avatars' })
const app = express.Router()

app.get('/profile', auth.isLoggedIn ,UserCtrl.profile)
app.post('/update/profile', auth.isLoggedIn, uploader.single('avatar'), UserCtrl.updateProfile)
// API

app.get('/api/auth', UserCtrl.isAuth)
app.get('/api/user', UserCtrl.getUser)

module.exports = app
