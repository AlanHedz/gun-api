'use strict'

const express = require('express')
const MessageCtrl = require('../../controllers/message')
const auth = require('../../middlewares/auth')
const app = express.Router()
const multer = require('multer')
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/messages')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
const uploader = multer({storage: storage})

app.get('/api/messages', MessageCtrl.getMessages)
app.post('/api/messages', auth.isLoggedIn, uploader.single('image'), MessageCtrl.saveMessage)

module.exports = app