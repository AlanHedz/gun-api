'use strict'

const express = require('express')
const HomeCtrl = require('../../controllers/home')
const app = express.Router()

app.get('/', HomeCtrl.home)
app.get('/homepage', HomeCtrl.homepage)

module.exports = app