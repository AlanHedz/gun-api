module.exports = function (app, passport) {
	app.post('/users/login', passport.authenticate('local-login', {
		successRedirect: '/homepage',
		failureRedirect: '/',
		failureFlash: true
	}))

	app.post('/users/register', passport.authenticate('local-signup', {
		successRedirect: '/homepage',
		failureRedirect: '/',
		failureFlash: 'Hubo un error, intentalo de nuevo.'
	}))
	
	app.get('/users/logout', (req, res) => {
		req.logout()
		res.redirect('/')
	})
}