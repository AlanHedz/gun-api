const path = require('path');
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const cssModules = 'modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]'

module.exports = {
    resolve: {
        extensions: ['*', '.jsx', '.js']
    },
    entry: ['./client/index.jsx'],
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'js/app.js'
    },
    module: {
        loaders: [
            { test: /(\.js|jsx)$/, exclude: /node_modules/, loader:'babel-loader', query:{ presets: ['babel-preset-env', 'react'] } },
            { test: /\.css$/, loader: `style-loader!css-loader?${cssModules}` }
        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css', { allChunks: true })
    ]
}