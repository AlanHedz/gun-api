import React, { Component } from 'react'
import styles from './profile-bar.css'

class ProfileBar extends Component {

    constructor (props) {
        super(props)
    }

    render() {
        const urlProfile = `/avatars/${this.props.avatar}` 
        return (  
            <div className='col s12 .hide-on-med-and-down'>
                <div className={`${styles.root} hoverable`}>
                    <div className='row no-margin valign-wrapper'>
                        <div className='col s12 text-center'>
                            <img src={urlProfile} className={styles.avatar}/>
                            <p className={styles.username}>{this.props.username}</p>
                            <a className='blue-text' href='#'><i className='fa fa-cog'></i> Configuracion</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
 
export default ProfileBar