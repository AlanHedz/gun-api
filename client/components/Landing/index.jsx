import React, { Component } from 'react'
import Login from '../Login'
import Signup from '../Signup'
import styles from './index.css'

class Landing extends Component {

    constructor () {
        super()
        this.state = {
            showForm: true
        }
        this.handleShowForm = this.handleShowForm.bind(this)
        this.handleHideForm = this.handleHideForm.bind(this)
    }

    handleShowForm (event) {
        event.preventDefault()
        this.setState({ showForm: true })
    }

    handleHideForm (event) {
        event.preventDefault()
        this.setState({ showForm: false })
    }

    renderForm () {
        if (this.state.showForm) {
            return ( <Login  hideForm={this.handleHideForm}/> )
        }
        return ( <Signup showForm={this.handleShowForm}/> )
    }

    render () {
        return(
            <div className={`container ${styles.landing}`}>
                <div className='row'>
                    <div className='col s10 push-s1'>
                        <div className='row'>
                            <div className='col m5 hide-on-small-only'>
                                <img src='http://www.freeiconspng.com/uploads/iphone-png-8.png' className={styles.iphone} />
                            </div>
                            { this.renderForm() }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Landing