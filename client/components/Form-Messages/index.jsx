import React, { Component } from 'react'
import styles from './form.css'

class FormMessages extends Component {

    constructor () {
        super()
    }

    render () { 
        return (  
            <div className='col l8 s12 m12'>
                <div className={styles.root}>
                    <form onSubmit={this.props.sendMessage}>
                        <div className='input-field no-margin'>
                            <textarea className='materialize-textarea' placeholder='Cuentanos que estas pasando' name='text'></textarea>
                        </div>  
                        <div className='file-field input-field'>
                            <div className='btn blue'>
                                <span>Portada</span>
                                <input type='file' name='image' />
                            </div>
                            <div className='file-path-wrapper'>
                                <input className="file-path validate" type="text" />
                            </div>
                        </div>
                        <div className='input-field text-right no-margin'>
                            <button className='btn btn-flat white orange-text' type='submit'>Enviar</button>
                            <a href='#' onClick={this.props.closeText} className='btn btn-flat white red-text'>Cerrar</a>
                        </div>                      
                    </form>
                </div>
            </div>
        )
    }
}
 
export default FormMessages;