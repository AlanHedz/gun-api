import React, { Component } from 'react'
import { render } from 'react-dom'

export class Footer extends Component {
    render() {
        return (
            <footer className='site-footer'>
                <div className='container'>
                    <div className='row'>
                        <div className='col s12 l3 center-align'>
                            <a href='#' data-activates='dropdown1' className='dropdown-button btn btn-flat grey lighten-5'>
                                Información
                            </a>
                            <ul id='dropdown1' className='dropdown-content uppercase'>
                                <li><a className='blue-text'>Asistencía</a></li>
                                <li><a className='blue-text'>Condiciones</a></li>
                                <li><a className='blue-text'>Privacidad</a></li>
                            </ul>
                        </div>
                        <div className='col s12 l3 push-l6 center-align'>© 2018 Gun</div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer