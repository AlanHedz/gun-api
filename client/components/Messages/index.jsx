import React, { Component } from 'react'
import styles from './messages.css'
import moment from 'moment'

class Messages extends Component {
    render() { 
        let dateFormat = moment(this.props.created).fromNow()
        let userPhoto = `/avatars/${this.props.creator.local.avatar}`
        let divStyle = {
            backgroundImage: 'url('+ userPhoto +')',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            width: '60px',
            height: '60px'
        }
        return (
            <div className='col l8'>
                <div className={`${styles.root} hoverable`} onClick={this.props.onModal}>
                        <div className={`${styles.avatar} inline-block`}>
                            <div style={divStyle} className='circle'></div>
                        </div>
                        <div className={`${styles.content} inline-block`}>
                            <div className='row no-margin'>
                                <div className='col s6'>
                                    <p className='no-margin blue-text'><a href='#'>{this.props.creator.local.username}</a></p>
                                </div>
                                <div className='col s6 text-right'>
                                    <small>{dateFormat}</small>
                                </div>
                            </div>
                            <div className={`${styles.text}`}>
                                <p className={styles.message}>{ this.props.text }</p>
                                <img src={this.props.image} className={styles.image}/>
                            </div>
                        </div>
                </div>
            </div>

        )
    }
}
 
export default Messages