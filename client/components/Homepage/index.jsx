import React, { Component } from 'react'
import axios from 'axios'
import alertify from 'alertify.js'

import Messages from '../Messages'
import Form from '../Form-Messages'
import ProfileBar from '../ProfileBar'
import Header from '../Header'
import MessageModal from '../Message-Modal'

class Homepage extends Component {
    constructor (props) {
        super(props)
        this.state = {
            messages: [],
            user_id: '',
            username: '',
            avatar: '',
            openText: false,
            message: {},
            openModal: false,
        }
        this.sendMessage = this.sendMessage.bind(this)
        this.getUser = this.getUser.bind(this)
        this.getMessages = this.getMessages.bind(this)
        this.handleCloseText = this.handleCloseText.bind(this)
        this.handleOpenText = this.handleOpenText.bind(this)
    }

    getMessages () {
        let BaseURL = 'http://localhost:3000'
        axios.get(`${BaseURL}/api/messages`)
            .then((res) => {
                this.setState({ messages: res.data })
            })
            .catch((err) => {
                console.log(err)
            })
    }

    getUser () {
        let BaseURL = 'http://localhost:3000'
        axios.get(`${BaseURL}/users/api/user`)
            .then((res) => {
                this.setState({ 
                    user_id: res.data._id,
                    username: res.data.local.username,
                    avatar: res.data.local.avatar,
                })
            })
            .catch((err) => {
                console.log(err)
            })
    }

    componentDidMount () {
        this.getMessages()
        this.getUser()
    }

    sendMessage (event) {
        event.preventDefault();
        let BaseURL = 'http://localhost:3000'

        const data = new FormData()
        data.append('image', event.target.image.files[0])
        data.append('user_id', this.state.user_id)
        data.append('text', event.target.text.value)
        
        axios.post(`${BaseURL}/api/messages`, data).then((res) => {
            this.getMessages()
            alertify.success('Noticía publicada correctamente.')
            this.setState({ openText: false })
        }).catch((err) => {
            alertify.error('Hubo un error al publicar la noticia, intentalo de nuevo.')
            console.log(err)
        })
    }

    handleOpenText (event) {
        event.preventDefault()
        this.setState({ openText: true })
    }

    handleCloseText (event) {
        event.preventDefault()
        this.setState({ openText: false })
    }

    handleShowModal (msg) {
        this.setState({ openModal:true, message: msg })
        $('.modal').modal()
        $('#showMessage').modal('open')
    }

    renderModal () {
        if (this.state.openModal) {
            return ( <MessageModal message={this.state.message} creator={this.state.message._creator} /> )
        }
    }

    renderOpenText () {
        if (this.state.openText) {
            return ( 
                <Form sendMessage={this.sendMessage}
                    closeText={this.handleCloseText}
                /> 
            )
        }
    }

    render () { 
        return ( 
            <div>
                <Header />
                <div className='container timeline'>
                    <div className='left profile-bar'>
                        <div className='row no-margin'>
                            <ProfileBar username={this.state.username}
                                avatar={this.state.avatar}
                            />
                        </div>
                    </div>
                    <div className='right messages-list'>
                        <div className='row'>
                            { this.renderOpenText() }
                            {this.state.messages.map(msg => {
                                return <Messages 
                                            text={msg.text}
                                            created={msg.created}
                                            creator={msg._creator}
                                            image={msg.imageUrl}
                                            onModal={() => this.handleShowModal(msg)}
                                            key={msg._id}
                                        />
                            })}
                        </div>
                    </div>
                </div>
                <div className='fixed-action-btn position-btn'>
                    <a className='btn-floating btn-large pink' onClick={this.handleOpenText}>
                        <i className='fa fa-pencil'></i>
                    </a>
                </div>
                { this.renderModal() }
            </div>
        )
    }
}
 
export default Homepage