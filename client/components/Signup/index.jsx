import React, { Component } from 'react'
import styles from './signup.css'

class Signup extends Component {
    render () {
        return(
            <div className='col s12 m7 l7'>
                <div className='row'>
                    <div className={styles.loginbox}>
                        <h1 className={styles.gun}>Gun</h1>
                        <form className={styles.loginform} action='/users/register' method='POST'>
                            <h2>Registrate para ver todas las noticias recientes</h2>
                            <div className='section'>
                                <a href='#' className={`btn ${styles.btnfb} hide-on-small-only`}>Iniciar Sesión con Facebook</a>
                                <a href='#' className={`btn ${styles.btnfb} hide-on-med-and-up`}><i className='fa fa-facebook-official'></i></a>
                            </div>
                            <div className='divider'></div>
                            <div className='section'>
                                <input type='email' name='email' placeholder='Correo electronico'/>
                                <input type='text' name='username' placeholder='Nombre de usuario' />
                                <input type='password' name='password' placeholder='Contraseña' />
                                <button type='submit' className={`btn waves-effect waves-light ${styles.btnlogin}`}>Registrate</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div className='row'>
                    <div className={styles.loginbox}>
                        ¿Tienes una cuenta? <a href='#' onClick={this.props.showForm}>Entrar</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default Signup