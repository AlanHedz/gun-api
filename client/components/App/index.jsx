import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import axios from 'axios'
import styles from './app.css'

import Header from '../Header'
import Homepage from '../Homepage'
import Landing from '../Landing'
import Footer from '../Footer'

class App extends Component {

    constructor () {
        super()
    }

    render () {
        return ( 
            <Router>
                <div>
                    <Route exact path='/' render={() => {
                        return (<Landing />)
                    }} />
                    <Route path='/homepage' render={() => {
                        return ( <Homepage /> )
                    }}/>
                </div>
            </Router>
        )
    }
}
 
export default App