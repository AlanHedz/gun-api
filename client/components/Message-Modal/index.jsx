import React, { Component } from 'react'
import styles from './modal.css'
import moment from 'moment'

class MessageModal extends Component {

    constructor () {
        super()
    }

    render () {
        let dateFormat = moment(this.props.message.created).fromNow()
        let userPhoto = `/avatars/${this.props.creator.local.avatar}`
        let divStyle = {
            backgroundImage: 'url('+ userPhoto +')',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            width: '60px',
            height: '60px'
        }
        return (
            <div id='showMessage' className='modal'>
                <div className='container'>
                    <div className='modal-content'>
                        <div className={`${styles.avatar} inline-block`}>
                            <div style={divStyle} className='circle'></div>
                        </div>
                        <div className={`${styles.content} inline-block`}>
                            <div className='row no-margin'>
                                <div className='col s6'>
                                    <p className='no-margin blue-text'><a href='#'>{this.props.creator.local.username}</a></p>
                                </div>
                                <div className='col s6 text-right'>
                                    <small>{dateFormat}</small>
                                </div>
                            </div>
                            <div className={`${styles.text}`}>
                                { this.props.message.text }
                                <img src={this.props.message.imageUrl} className={styles.image} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default MessageModal