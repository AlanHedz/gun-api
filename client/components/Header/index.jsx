import React, { Component } from 'react'
import axios from 'axios'

class Header extends Component {

    constructor (props) {
        super(props)
        this.state = {
            isLoggedIn: false
        }
        this.openDropdwon = this.openDropdwon.bind(this)
    }

    componentDidMount () {
        let BaseURL = 'http://localhost:3000'
        axios.get(`${BaseURL}/users/api/auth`)
            .then((res) => {
                this.setState({ isLoggedIn: res.data })
            })
            .catch((err) => {
                console.log(err)
            })  
    }

    openDropdwon (event) {
        event.preventDefault()
        $('#user').dropdown('open')
    }

    render () { 
        return (
            <div className='navbar-fixed'> 
            <nav className='header'>
                <div className='nav-wrapper'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col s12 m6'>
                                <a href='/' className='brand-logo reactrr'>Gun</a>
                            </div>
                            <div className='col s2 m6'>
                                    <ul className='right list-nav'>
                                        <li><a className='dropdown-button btn btn-large btn-flat white' data-activates='user' onClick={this.openDropdwon}><i className='fa fa-user icon-nav' aria-hidden="true"></i></a></li>
                                        <ul className='dropdown-content uppercase' id='user'>
                                            <li><a href='/users/logout' className='uppercase'>Salir</a></li>
                                        </ul>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            </div>
        )
    }
}
 
export default Header