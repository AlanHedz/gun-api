import React, { Component } from 'react'
import { render } from 'react-dom'
import App from './components/App'
import Footer from './components/Footer'
import moment from 'moment'

moment.locale('es')

render(<App />, document.getElementById('app'))
render(<Footer />, document.getElementById('footer'))